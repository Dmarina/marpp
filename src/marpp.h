#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;

template<class T, int length, class MatrixPoliciy>
  class array
{
  	template<class U, int l>
  	friend ostream &operator<<(ostream &output, array<U,l> &a ); // Overload cout<<

	template<class U, int l>
  	friend int size(array<U,l> &a ); // This function gives the dimension of the array

	template<class U, int l>
  	friend array<U,l> sqrt(array<U,l> &a );

   	private:
  	T *v;

	public:
	array(); // Default constructor
	array(int num); // This constructor is filled with num
	int dimension(); // This function gives the dimension of the array
	~array(); // Destructor

	T &operator[]( int );
};
