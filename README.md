Meta Array Library  (MARpp)
==========================

This is a project for developing a Meta Library for arrays in C++.

All this files are protected by a free license. For details, see the file [LICENSE](LICENSE).
