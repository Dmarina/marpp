//,--------------
//| Static arrays
//`--------------
#include <iostream>
#include <marpp>
using namespace std;   // Just for testing
using namespace marpp;

int main()
{
  array<double, dimension(10)> x;
  array<int, dimension(10)> irange;

  // Use a loop for fill irange array and compute sqrt(i) of each i<n
  int n = size(x);
  for(int i=0, i<n, i++)
    {
      irange(i) = i;
      x(i) = sqrt(i);  // sqrt should work on integers
    }
  cout << "x=" << x << endl;

  // Compute sqrt(x) without loops
  x = sqrt(irange);
  cout << "x=" << x << endl;
}
