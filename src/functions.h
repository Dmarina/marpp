#include<iostream>
using namespace std;


template<class T, int length> // Default constructor
array<T,length>::array()
{	
	v=(T*)malloc(length*sizeof(T));
}

template<class T, int length> // Constructor what fills the array with num
array<T,length>::array(int num)
{
	v=(T*)malloc(length*sizeof(T));
	for(int i=0;i<length;i++)
		v[i]=num;
}


template<class T, int length> // Destructor
array<T,length>::~array()
{
	free(v);
}

template<class T, int length>
int array<T,length>::dimension()
{
	return(length);
}

template<class T, int length>
T &array<T,length>::operator[](int i)
{
	return(v[i]);
}


//  vector=(int*) calloc (i,sizeof(int));
//   vector= (int*) realloc (vector, 3 * sizeof(int))
