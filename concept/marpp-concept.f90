!
! Static arrays
!
program marpp_concepto
  ! Declaration of variables
  implicit none
  real(kind=8), dimension(10) :: x
  integer, dimension(10) :: irange
  integer :: i, n

  ! Use a loop for fill irange array and compute sqrt(i) of each i<n
  n = size(x)
  do i = 1, n
     irange(i) = i
     x(i) = sqrt(real(i))  ! sqrt must operate on real numbers
  enddo
  print *, "x=", x

  ! Compute sqrt(x) without loops
  x = sqrt(real(irange))
  print *, "x=", x
end program marpp_concepto
